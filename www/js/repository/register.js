BhromonApp.factory(
	'register', 
	['$resource',
		function($resource) {
            return $resource('',{},{
			    get:{
			        url:'http://dev.arrels-ws.pre.basetis.com/getSpots/7b385f10f66982a24fc09f39cc84194a.json?email=:email', 
			        params: { email: "@email" },
			        method:'GET',
			        isArray:true
			    },
			    getById:{
			        url:'http://dev.arrels-ws.pre.basetis.com/getSpots/7b385f10f66982a24fc09f39cc84194a.json?id=:id', 
			        params: { id: "@id" },
			        method:'GET',
			        isArray:true
			    },
			    updateFeedback:{
			    	url:'http://dev.arrels-ws.pre.basetis.com/feedback',
			    	params: {
						'id': '@rowid',
						'feedback': '@text',
						'feedbackUser': '@usuari'
					},
					isArray:false,
					method:'POST', 

			    }
			})
		}
	]
);

